//Ilona Skarp Noroff academy third coding Kata
//Task is to take userinput word and test if it's a palindrome
//spaces are ignored
#include <QCoreApplication>
#include <QtDebug>
#include <QString>
#include <QTextStream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    /*std::string*/ QString word;

      /*std::cout*/ QTextStream(stdout)<<"Test if your word is a palindrom";//<<std::endl;
      //std::getline(std::cin, word);
          QTextStream stream(stdin);
          QString line;
          while (stream.readLineInto(&line)) {
              word += line;
          }
      for (int i = 0; i < word.length(); i++)                                                      // Converting uppercase letters to lowercase
          {
              word[i] = word[i].toLower();
          }


      bool palindrome = false;
      int startindex = 0;
      int endindex =word.length()-1;
      for(int i = 0; i < floor(word.length()/2); i++) {
          Q_ASSERT(i >= 0);
          if(endindex -1 <= startindex) {
               palindrome = true;
               break;
          }
         if(word[startindex]==' ') {
              startindex++;
          } else if (word[endindex]== ' '){
              endindex--;
          }
          if(word[startindex]== word[endindex]) {
              palindrome = true;
          } else {
              palindrome = false;
              break;
          }
          startindex = startindex+1;
          endindex = endindex-1;
      }
      if(palindrome == true) {
          /*std::cout*/qDebug() << "It is a palindrome indeed!"; //<<std::endl;
      } else {
          /*std::cout*/QTextStream(stdout) << "It is not a palindrome. Damn!!"; //<<std::endl;
      }



        return a.exec();
       // return 0;
}
