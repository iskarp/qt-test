#include<iostream>
#include<string>
#include<cmath>
#include <QCoreApplication>
#include <QtDebug>
#include <QString>
#include <QTextStream>
//find the int form number for the number string
int find_number_to_word(/*std::string*/QString word) {

    int result =-1;


    //check which number the word is. Between 0 and 20
    if(word == "zero") {
        result = 0;
    } else if (word== "one") {
        result = 1;
    } else if(word == "two") {
        result = 2;
    } else if (word == "three") {
        result = 3;
    } else if (word == "four") {
        result = 4;
    } else if (word == "five") {
        result = 5;
    } else if (word == "six") {
        result = 6;
    } else if (word == "seven") {
        result = 7;
    } else if (word == "eight") {
        result = 8;
    } else if (word == "nine") {
        result = 9;
    } else if (word == "ten") {
        result = 10;
    } else if (word == "eleven") {
        result = 11;
    }else if (word == "twelwe") {
        result = 12;
    }else if (word == "thirteen") {
        result = 13;
    }else if (word == "fourteen") {
        result = 14;
    }else if (word == "fifteen") {
        result = 15;
    }else if (word == "sixteen") {
        result = 16;
    } else if (word == "seventeen") {
        result = 17;
    } else if (word == "eighteen") {
        result = 18;
    } else if (word == "nineteen") {
        result = 19;
    }else if (word == "twenty") {
        result = 20;
    }
    return result;

}



/*std::string*/QString calculate_numbers(/*std::string*/QString word) {


    /*std::string*/QString number1 = "";
    /*std::string*/QString number2 = "";
    /*std::string*/QString resultstr = "";
    /*std::string*/QString temp = "";
    int n = 0;
    int result = 0;
    int result2 = 0;
    //find first number and represent it as string
    while(n < word.length()) {
        if(word[n]== ' ') {
            break;
        } else {
            temp += word[n];
        }
        n++;
    }
    number1 = temp;
    temp = "";
    n = 0;
    int index =0;
    //find second number and represent it as a string
     while(n < word.length()) {
        if(word[n]== ' ') {
            index++;
        } else if(index == 2){
            temp += word[n];
        }
        n++;
    }
    number2 = temp;

    //convert string numbers to int
    int first = find_number_to_word(number1);
    int second = find_number_to_word(number2);

    n =0;
    index = 0;
    /*std::string*/QString temp3;
    //find operator word
    while(n < word.length()) {
        if (word[n]== ' ' && index >=1){
            break;
        }
        else if(word[n]== ' ' && index < 1) {
            index++;
        }else if(index == 1){
            temp3 += word[n];
        }
        n++;
    }
     /*std::string*/QString cal_operator = "";
     cal_operator += temp3;

    //calculate by operator choise
    if(cal_operator == "plus") {
        Q_ASSERT(cal_operator == "plus");
        result = first + second;
    } else if (cal_operator == "minus") {
         Q_ASSERT(cal_operator == "minus");
        result = first - second;
    } else if (cal_operator == "times") {
         Q_ASSERT(cal_operator == "times");
        result = first * second;
    } else if (cal_operator == "over") {
         Q_ASSERT(cal_operator == "over");
        result = first /second;
        result2 = first%second;
    }

    //format the result
    if(result2!=0) {
        resultstr += "Result: ";
        resultstr += std::to_string(result);
        resultstr += " Reminder: ";
        resultstr += std::to_string(result2);
    } else {
        resultstr += "Result: " + std::to_string(result);
    }


    return resultstr;

}


int main () {

    //console print for the program
    /*std::string*/ QString command = "";
    /*std::cout*/qDebug() << "Give a operation in form:"; //<<std::endl;
   /* std::cout*/qDebug() <<"five times six"; //<< std::endl;
    /*std::cout*/qDebug() << "Numbers form 0 to 20 are allowed."; //<<std::endl;
    /*std::cout*/qDebug() << "Valid operations: plus, minus, times, over"; // <<std::endl;
    QTextStream stream(stdin);
    QString command;
    QString line;
    while (stream.readLineInto(&line)) {
        command += line;
    }
    //getline(std::cin, command);
    /*std::cout*/ QTextStream(stdout) << calculate_numbers(command);

    return 0;
}

